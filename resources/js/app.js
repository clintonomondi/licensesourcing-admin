import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input';
import Notifications from 'vue-notification';
import vueCountryRegionSelect from 'vue-country-region-select';
import JsonExcel from "vue-json-excel";
import moment from 'moment'
import VueGeolocation from 'vue-browser-geolocation';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'
import vSelect from "vue-select";

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import VueEditor from "vue2-editor";


Vue.component("v-select", vSelect);
Vue.use(VueToast);
Vue.use(VueEditor);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(vueCountryRegionSelect);
Vue.component("downloadExcel", JsonExcel);
Vue.use(VueGeolocation);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
    // We use `date-fns` under the hood
    // So you can use all locales from it
    // locales: {
    //     'zh-CN': require('date-fns/locale/zh_cn'),
    //     ja: require('date-fns/locale/ja')
    // }
});

import App from './view/App'
import Login from './pages/login'
import Home from './pages/home'
import Category from './pages/category'
import Products from './pages/products'
import Forget from './pages/forget'
import Users from './pages/users'
import Clients from './pages/clients'
import Invoice_Non_Dis from './pages/invoice-undisbursed'
import Invoice_Dis from './pages/invoice-disbursed'
import Invoice_Unpaid from './pages/invoice-unpaid'
import Password from './pages/password'
import MpesaLogs from './pages/mpesa-logs'
import PyapalLogs from './pages/paypal-logs'
import Error from './pages/error'
import Support from './pages/support'
import Visitors from './pages/visitors'



const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/logs/error',
            name: 'error',
            component: Error,
        },
        {
            path: '/logs/visitors',
            name: 'visitors',
            component: Visitors,
        },
        {
            path: '/logs/support',
            name: 'support',
            component: Support,
        },
        {
            path: '/logs/paypal',
            name: 'paypallogs',
            component: PyapalLogs,
        },
        {
            path: '/logs/mpesa',
            name: 'mpesalogs',
            component: MpesaLogs,
        },
        {
            path: '/password',
            name: 'password',
            component: Password,
            meta: { hideNavigation: true }
        },
        {
            path: '/invoices/unpaid',
            name: 'unpaid',
            component: Invoice_Unpaid,
        },
        {
            path: '/invoices/disbursed',
            name: 'disbursed',
            component: Invoice_Dis,
        },
        {
            path: '/products',
            name: 'products',
            component: Products,
        },
        {
            path: '/forget',
            name: 'forget',
            component: Forget,
            meta: { hideNavigation: true }
        },
        {
            path: '/category',
            name: 'category',
            component: Category,
        },
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { hideNavigation: true }
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
        },
        {
            path: '/clients',
            name: 'clients',
            component: Clients,
        },
        {
            path: '/invoices/non-disbursed',
            name: 'non-disbursed',
            component: Invoice_Non_Dis,
        },

    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});